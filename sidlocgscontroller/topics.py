"""Topics module"""
import datetime
import logging
from pathlib import Path

from sidlocgscontroller import jobs, settings

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions

LOGGER = logging.getLogger(__name__)


def schedule(message):  # pylint: disable=missing-function-docstring
    try:
        start_time = datetime.datetime.fromisoformat(
            message.payload['start_time'])
    except KeyError:
        start_time = None
    except ValueError as error:
        LOGGER.exception(error)
        LOGGER.error('Invalid start time! Ignoring message...')
        return

    job_args = [
        message.controller,
        message.payload['flowgraph'],
        f'{settings.FLOWGRAPHS_PATH}/{message.payload["flowgraph"]}.py',
    ]
    try:
        job_args.append(
            datetime.datetime.fromisoformat(message.payload['end_time']))
    except ValueError as error:
        LOGGER.exception(error)
        LOGGER.error('Invalid end time! Ignoring message...')
        return

    job_kwargs = {}
    try:
        file_path = Path(message.payload['parameters']['file'])
    except KeyError:
        pass
    else:
        message.payload['parameters']['file'] = str(
            Path(settings.DEFAULT_OUTPUT_PATH).joinpath(file_path))
    try:
        job_kwargs['parameters'] = message.payload['parameters']
    except KeyError:
        pass

    jobs.SCHEDULER.add_job(jobs.enable_flowgraph,
                           trigger='date',
                           args=job_args,
                           kwargs=job_kwargs,
                           id='enable_flowgraph',
                           misfire_grace_time=3,
                           coalesce=True,
                           max_instances=1,
                           replace_existing=True,
                           run_date=start_time)


def files(message):  # pylint: disable=missing-function-docstring
    try:
        path = Path(settings.DEFAULT_OUTPUT_PATH).joinpath(
            message.payload['remove'])
    except KeyError:
        pass
    else:
        jobs.SCHEDULER.add_job(jobs.remove_file,
                               args=[path],
                               id='remove_file',
                               misfire_grace_time=None,
                               coalesce=False,
                               max_instances=1)

    try:
        path = Path(settings.DEFAULT_OUTPUT_PATH).joinpath(
            message.payload['upload'])
    except KeyError:
        pass
    else:
        job_kwargs = {}
        for k, v in {
                'headers': 'headers',
                'timeout': 'timeout',
                'start': 'start',
                'end': 'end',
                'compress': 'compressed',
        }.items():
            try:
                job_kwargs[v] = message.payload[k]
            except KeyError:
                pass

        jobs.SCHEDULER.add_job(jobs.upload_file,
                               args=[path, message.payload['url']],
                               kwargs=job_kwargs,
                               id='upload_file',
                               misfire_grace_time=None,
                               coalesce=False,
                               max_instances=10000,
                               executor='upload')
