"""SIDLOC Ground Station Controller module initialization"""
import logging
import multiprocessing
import signal
import sys

from sidlocgscontroller import settings
from sidlocgscontroller.controller import SidlocController

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions

logging.basicConfig(format=settings.LOG_FORMAT,
                    level=getattr(logging, settings.LOG_LEVEL))
LOGGER = logging.getLogger(__name__)


def main():
    """SIDLOC Ground Station Controller"""
    controller = SidlocController(settings.MQTT_HOST,
                                  settings.MQTT_PORT,
                                  settings.MQTT_USERNAME,
                                  settings.MQTT_PASSWORD,
                                  tls=settings.MQTT_TLS)
    LOGGER.addHandler(controller.mqtt_log_handler)

    def signal_handler(_signum, _frame):
        if not multiprocessing.parent_process():
            controller.stop()
            sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    controller.start()
