"""Files module"""
import logging
import lzma
from pathlib import Path

import requests

LOGGER = logging.getLogger(__name__)


class SidlocFile():  # pylint: disable=missing-class-docstring

    def __init__(self, path, start=0, end=0, compressed=False):
        """Class contructor"""
        self.path = Path(path)
        self.start = start
        self.end = end
        self._compressed = compressed
        self._data = None

    def remove(self):
        """Remove file
        """
        try:
            self.path.unlink()
        except FileNotFoundError:
            LOGGER.error("File '%s' does not exist!", self.path.name)
            return
        LOGGER.debug("File '%s' removed.", self.path.name)

    @property
    def data(self):  # pylint: disable=missing-function-docstring
        if self._data:
            return self._data
        if not self._compressed:
            self._data = self._read()
        else:
            self._data = self._compress(self._read())
        return self._data

    def upload(self, url, headers=None, timeout=300):  # pylint: disable=missing-function-docstring  # noqa: E501
        if self.data:
            LOGGER.debug("Uploading '%s' file...", self.path.name)
            try:
                requests.put(url,
                             headers=headers,
                             data=self.data,
                             timeout=timeout)
            except requests.exceptions.ConnectionError as error:
                LOGGER.exception(error)
            else:
                LOGGER.debug("Uploaded '%s' file'.", self.path.name)

    def _read(self):
        LOGGER.debug("Reading file '%s' from %s to %s...", self.path.name,
                     self.start or 'start', self.end or 'end')
        try:
            fil = open(self.path, 'rb')
        except FileNotFoundError:
            LOGGER.error("File '%s' does not exist!", self.path.name)
            return None
        with fil:
            if self.start:
                fil.seek(self.start)
            return fil.read(self.end -
                            self.start if self.end > self.start else -1)

    def _compress(self, data_in):
        LOGGER.debug("Compressing file '%s' from %s to %s...", self.path.name,
                     self.start or 'start', self.end or 'end')
        data_out = lzma.compress(data_in)
        LOGGER.debug(
            "File '%s' compressed from %s to %s; "
            'original size: %s, compressed size: %s.', self.path.name,
            self.start or 'start', self.end or 'end', len(data_in),
            len(data_out))
        return data_out


class SidlocDirectory():  # pylint: disable=too-few-public-methods, missing-class-docstring  # noqa: E501

    def __init__(self, path):
        """Class constructor"""
        self._path = Path(path)

    @property
    def files(self):
        """Get list of files in directory

        :return: List of files in directory
        :rtype: list of str
        """
        return [{
            'name': _.name,
            'size': _.stat().st_size,
        } for _ in self._path.iterdir() if _.is_file()]
