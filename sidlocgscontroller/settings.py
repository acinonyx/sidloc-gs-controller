"""Settings module"""
from os import environ

from dotenv import load_dotenv

try:
    load_dotenv()
except EnvironmentError:
    # XXX: Workaround for readthedocs  # pylint: disable=fixme
    pass


def strtobool(val):  # pylint: disable=missing-function-docstring
    val = val.lower()
    if val in {'y', 'yes', 't', 'true', 'on', '1'}:
        return True
    if val in {'n', 'no', 'f', 'false', 'off', '0'}:
        return False
    raise ValueError(f'invalid truth value {val!r}')


MESSAGE_SCHEMA_FILE = 'sidloc-gs-controller.schema.json'
MQTT_TOP_LEVEL_TOPIC = 'sidloc'

DEFAULT_OUTPUT_PATH = environ.get('SIDLOC_GS_CONTROLLER_DEFAULT_OUTPUT_PATH',
                                  '/var/lib/sidloc-gs-controller')
FLOWGRAPHS_PATH = environ.get('SIDLOC_GS_CONTROLLER_FLOWGRAPHS_PATH',
                              '/usr/local/bin')
MQTT_HOST = environ.get('SIDLOC_GS_CONTROLLER_MQTT_HOST', 'mqtt.satnogs.org')
MQTT_PORT = int(environ.get('SIDLOC_GS_CONTROLLER_MQTT_PORT', '1883'))
MQTT_USERNAME = environ.get('SIDLOC_GS_CONTROLLER_MQTT_USERNAME', None)
MQTT_PASSWORD = environ.get('SIDLOC_GS_CONTROLLER_MQTT_PASSWORD', None)
MQTT_TLS = strtobool(environ.get('SIDLOC_GS_CONTROLLER_MQTT_TLS', 'False'))
LOG_FORMAT = environ.get('SIDLOC_GS_CONTROLLER_LOG_FORMAT',
                         '%(name)s - %(levelname)s - %(message)s')
LOG_LEVEL = environ.get('SIDLOC_GS_CONTROLLER_LOG_LEVEL', 'WARNING')
SCHEDULER_LOG_LEVEL = environ.get('SIDLOC_GS_CONTROLLER_SCHEDULER_LOG_LEVEL',
                                  'WARNING')
