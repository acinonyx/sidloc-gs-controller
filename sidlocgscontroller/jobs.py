"""Jobs module"""
import datetime
import json
import logging

from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.schedulers.background import BackgroundScheduler

from sidlocgscontroller import settings
from sidlocgscontroller.files import SidlocDirectory, SidlocFile
from sidlocgscontroller.flowgraph import SidlocFlowgraph

from ._version import get_versions

__version__ = get_versions()['version']

del get_versions

LOGGER = logging.getLogger(__name__)
logging.getLogger('apscheduler').setLevel(
    getattr(logging, settings.SCHEDULER_LOG_LEVEL))

SCHEDULER = BackgroundScheduler(executors={
    'default': ThreadPoolExecutor(10),
    'upload': ThreadPoolExecutor(1),
})


def status(state):  # pylint: disable=missing-function-docstring
    return ('status', {
        'timestamp': datetime.datetime.now(tz=datetime.UTC).isoformat(),
        'version': __version__,
        'status': state,
        'files': SidlocDirectory(settings.DEFAULT_OUTPUT_PATH).files,
    })


def enable_flowgraph(  # pylint: disable=missing-function-docstring
        controller,
        name,
        module_path,
        end_time,
        parameters=None):
    try:
        controller.flowgraph = SidlocFlowgraph(name, module_path, **parameters)
    except FileNotFoundError as error:
        LOGGER.error(error)
        LOGGER.error('Could not load flowgraph, skipping job...')
        return

    controller.flowgraph.enabled = True
    topic, payload = status('running')
    controller.mqtt_client.publish(
        f'{settings.MQTT_TOP_LEVEL_TOPIC}/'
        f'{controller.mqtt_client.username}/{topic}',
        json.dumps(payload),
        qos=2)

    SCHEDULER.add_job(disable_flowgraph,
                      trigger='date',
                      args=[controller.flowgraph],
                      id='disable_flowgraph',
                      misfire_grace_time=None,
                      coalesce=True,
                      max_instances=1,
                      replace_existing=True,
                      run_date=end_time)
    controller.flowgraph.wait()
    topic, payload = status('idle')
    controller.mqtt_client.publish(
        f'{settings.MQTT_TOP_LEVEL_TOPIC}/'
        f'{controller.mqtt_client.username}/{topic}',
        json.dumps(payload),
        qos=2)


def disable_flowgraph(flowgraph):  # pylint: disable=missing-function-docstring
    if flowgraph:
        flowgraph.enabled = False


def heartbeat(client):  # pylint: disable=missing-function-docstring
    topic, payload = status('online')
    client.publish(
        f'{settings.MQTT_TOP_LEVEL_TOPIC}/{client.username}/{topic}',
        json.dumps(payload),
        qos=2)


def remove_file(path):  # pylint: disable=missing-function-docstring
    SidlocFile(path).remove()


def upload_file(path, url, **kwargs):  # pylint: disable=missing-function-docstring  # noqa: E501
    file_kwargs = {
        k: kwargs[k]
        for k in ('start', 'end', 'compressed') if k in kwargs
    }
    upload_kwargs = {
        k: kwargs[k]
        for k in ('headers', 'timeout') if k in kwargs
    }
    SidlocFile(path, **file_kwargs).upload(url, **upload_kwargs)
