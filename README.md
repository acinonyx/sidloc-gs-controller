# SIDLOC Ground Station Controller #

Controller software for SIDLOC ground stations

## License ##

[![license](https://img.shields.io/badge/license-AGPL%203.0-6672D8.svg)](LICENSE)
[![Libre Space Foundation](https://img.shields.io/badge/%C2%A9%202024-Libre%20Space%20Foundation-6672D8.svg)](https://librespacefoundation.org/)
